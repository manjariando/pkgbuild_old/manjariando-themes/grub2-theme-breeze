# Maintainer: tioguda <guda.flavio@gmail.com>

pkgname=grub2-theme-breeze
pkgver=5.20.3
pkgrel=3
pkgdesc="A minimalistic GRUB theme inspired by Breeze."
arch=("any")
url="https://www.gnome-look.org/p/1000140"
license=('CC BY-SA')
makedepends=('imagemagick')
install=grub-theme.install
source=("${pkgname}-${pkgver}.tar.gz::https://github.com/gustawho/${pkgname}/archive/${pkgver}.tar.gz"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/grub.png")
sha256sums=('0431817cc653ff112b4b7b9dbafe3267e9c1e12e44a8bbd48ca2d5019301cd2e'
            '1ed8038d4e6bbb68385a96762882d4f18d339e75db43d2a9cdcab9ef8d88c638'
            'b013684ded755a4ad7029f70f51d8acafe494d7b9f8772674f6e7510791d043a')

prepare() {
    cd ${srcdir}/${pkgname}-${pkgver}
}

_breeze_desktop="[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=
Exec=
Categories=Utility;
NoDisplay=true"

build() {
    cd "${srcdir}"
    echo -e "$_breeze_desktop" | tee com.${pkgname//-/_}.desktop
}

package() {
    depends=('grub')

    cd ${srcdir}/${pkgname}-${pkgver}
    install -dm755 ${pkgdir}/boot/grub/themes/breeze
    cp -r breeze/* ${pkgdir}/boot/grub/themes/breeze
    
    # Fix icon to dual boot with two versions of Manjaro
    cp -r ${pkgdir}/boot/grub/themes/breeze/icons/manjaro.png ${pkgdir}/boot/grub/themes/breeze/icons/manjarolinux.png

    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"
    install -Dm644 "${srcdir}/${pkgname}-${pkgver}/LICENSE" "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
    install -Dm644 "${srcdir}/com.${pkgname//-/_}.desktop" \
        "${pkgdir}/usr/share/applications/com.${pkgname//-/_}.desktop"

    for size in 22 24 32 48 64 128; do
        mkdir -p "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
        convert "${srcdir}/grub.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/${pkgname}.png"
    done

    sed -i "s:PACKAGE=.*:PACKAGE=breeze:" "${startdir}/grub-theme.install"
}
